"use strict"
// Теоретичні питання 
// 1. Для створення рядка у JavaScript можна використовувати одинарні (''), подвійні ("") або зворотні (``) лапки.
let singleQuotesString = 'Це рядок з одинарними лапками.';
let doubleQuotesString = "Це рядок з подвійними лапками.";
let backticksString = `Це рядок з зворотніми лапками.`;

// 2. Різниця між одинарними, подвійними та зворотніми лапками в JavaScript полягає 
// у використанні спеціальних символів та інтерполяції значень.
// Одинарні та подвійні лапки використовуються для звичайних рядків, 
// а зворотні лапки дозволяють вставляти значення змінних за допомогою ${...} та використовувати рядок-шаблон.

// 3. Для перевірки, чи два рядки рівні між собою,
// можна використовувати оператор строгого порівняння (===) 
// або методи порівняння рядків, такі як localeCompare().
let string1 = 'Рядок 1';
let string2 = 'Рядок 2';
console.log(string1 === string2); // Поверне false, оскільки рядки різні.

// 4. Date.now() повертає кількість мілісекунд,
// що пройшли з 1 січня 1970 року 00:00:00 UTC до поточного моменту часу.
let currentTime = Date.now();

// 5. Date.now() повертає час у мілісекундах, 
// тоді як new Date() повертає об'єкт типу Date, 
// який містить повну дату та час в поточному часовому поясі.
let currentDate = new Date();

// Практичне завдання 1

function isPalindrome(str) {
    
    let alphanumericStr = str.replace(/[\W_]/g, '').toLowerCase();
    
    let reversedStr = alphanumericStr.split('').reverse().join('');
    
    return alphanumericStr === reversedStr;
}

let inputString = prompt('Введіть рядок для перевірки на паліндром:');
if (isPalindrome(inputString)) {
    alert('Введений рядок є паліндромом.');
} else {
    alert('Введений рядок не є паліндромом.');
}

// Практичне завдання 2

function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}

let inputString2 = prompt('Введіть рядок для перевірки:');
let maxLength = parseInt(prompt('Введіть максимальну довжину рядка:'));

if (checkStringLength(inputString2, maxLength)) {
    alert('Рядок коротше або дорівнює вказаній максимальній довжині.');
} else {
    alert('Рядок довший за вказану максимальну довжину.');
}

// Практичне завдання 3

function calculateAge(birthDate) {
    // Поточна дата
    let currentDate = new Date();
    
    let age = currentDate.getFullYear() - birthDate.getFullYear();
    
    if (currentDate.getMonth() < birthDate.getMonth() || (currentDate.getMonth() === birthDate.getMonth() && currentDate.getDate() < birthDate.getDate())) {
        age--;
    }
    
    return age;
}

let birthDateString = prompt('Введіть дату народження в форматі YYYY-MM-DD:');
let birthDate = new Date(birthDateString);

let age = calculateAge(birthDate);
alert(`Ваш вік: ${age} років.`);